// modules
import { useEffect, useRef, useState } from 'react';
// functions
import { buildFormData } from '../functions';
// constants
import { GET } from '../constants/methods';
import { FILE_TYPE, JSON_TYPE } from '../constants/requestType';
import Config from '../config.json';

/**
 * @returns [
 *  result: object, // ajax response
 *  load: function, // launch ajax request
 * 	loading: boolean
 * ]
 */
const useFetch = (firstCallPriority = true) => {
	const [result, setResult] = useState(null);
	const [loading, setLoading] = useState(false);
	const unmounted = useRef(false);
	// If component is unmounted result can't be updated

	// useEffect(() => {
	//     return () => {
	//     };
	// }, []);

	useEffect(() => {
		if (result) {
			if (loading) {
				setLoading(false);
			}
			if (!result.success && result.message) {
				// dispatch(addMessage(result.message, false));
			}
		}
	}, [result]);

	/**
	 * @param {{
	 *  url: string,
	 *  method: string|undefined,
	 *  body: object|undefined,
	 *  token: string|undefined,
	 *  contentType: string|undefined
	 * }}
	 */
	const load = async ({ url = '', method = GET, body, contentType = JSON_TYPE }) => {
		if (loading) {
			if (firstCallPriority) return;
		}

		if (!unmounted.current) setLoading(true);

		const params = {
			method,
			headers: {},
		};
		if (body) {
			// console.log('body', body);
			if (contentType === JSON_TYPE) {
				body = JSON.stringify(body);
				params.headers['Content-Type'] = contentType;
			} else if (contentType === FILE_TYPE) {
				body = buildFormData(body);
			}
			params.body = body;
		}

		// Catch if token is invalid/expired.
		// Then try to get a new one with refreshToken : success ? reload : logout
		try {
			const response = await manageFetch(url, params);
			setResult(response);
		} catch (e) {
			console.log(e);
		}
	};
	return [result, load, loading];
};

export const manageFetch = async (url, params) => {
	// If fetch, repsonse status or .json() is an error throw error to manageError
	let response = null;
	try {
		response = await fetch(`${Config.BASE_URL_API}/api/${url}`, params);
		// console.log('response', response);
		if (response.ok && response.status >= 200 && response.status < 300) {
			if (response.headers.get('Content-Type') === JSON_TYPE) {
				const responseJson = await response.json();
				// console.log('responseJson', responseJson);
				responseJson.success = true;
				responseJson.code = response.status;
				return responseJson;
			}
			if (response.headers.get('Content-Type').includes('text/html')) {
				const error = new Error('Erreur : ressource introuvable.');
				error.code = 404;
				throw error;
			}
			const responseBlob = await response.blob();
			responseBlob.success = true;
			responseBlob.code = response.status;
			return responseBlob;
		}
		throw await response.json();
	} catch (e) {
		// Set as result
		if (response && response.code && typeof e === 'object' && !e.code) {
			e.code = response.code;
		}
		return manageError(e);
	}
};

export const manageError = error => {
	if (!error) error = {};
	error.success = false;

	if (error.message === 'TODO ') {
		// Throw error to refreshToken
		throw error;
	}

	// Set as result
	return error;
};

export default useFetch;
