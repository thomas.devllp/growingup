import { useLocation } from 'react-router';

/**
 * @returns
 *  currentUrlQuery: object,
 */

const useQuery = () => {
	const { search } = useLocation();
	//SubString: retire "?"
	return search
		.substring(1)
		.split('&')
		.reduce((paramsObject, param) => {
			const [key, value] = param.split('=');
			if (key && value) {
				paramsObject[key] = decodeURIComponent(value);
			}
			return paramsObject;
		}, {});
};

export default useQuery;
