import React from 'react';
import { useState, useEffect } from 'react';
import useFetch from '../useFetch';

const useGetFlowers = () => {
	const [flowers, setFlowers] = useState([]);
	const [result, load, loading] = useFetch();

	useEffect(() => {
		load({
			url: 'flower',
		});
	}, []);

	useEffect(() => {
		if (result) {
			if (result.success) {
				setFlowers(result.flowers);
			}
		}
	}, [result]);

	return [flowers, loading];
};

export default useGetFlowers;
