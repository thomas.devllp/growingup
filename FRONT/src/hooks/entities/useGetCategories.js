import React from 'react';
import { useState, useEffect } from 'react';
import useFetch from '../useFetch';

const useGetCategories = () => {
	const [categories, setCategories] = useState([]);
	const [result, load, loading] = useFetch();

	useEffect(() => {
		load({
			url: 'category',
		});
	}, []);

	useEffect(() => {
		if (result) {
			if (result.success) {
				setCategories(result.categories);
			}
		}
	}, [result]);

	return [categories, loading];
};

export default useGetCategories;
