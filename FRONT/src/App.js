import './scss/App.scss';
import PageRouter from './component/routers/PageRouter';

function App() {
	return (
		<div className="App">
			<header className=""></header>
			<PageRouter />
		</div>
	);
}

export default App;
