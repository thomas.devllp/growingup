import { configureStore } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import splash from './splash';
import user from './user';

const makeStore = () =>
	configureStore({
		reducer: {
			splash,
			user,
		},
	});

export const wrapper = createWrapper(makeStore);
