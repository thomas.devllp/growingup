import { createSlice } from '@reduxjs/toolkit';
import { ROLE_GUARD } from '../constants/role';

const initialState = {
	twister: '',
	email: '',
	role: ROLE_GUARD,
};

export const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		set: (state, action) => {
			return { ...state, ...action.payload };
		},
		setTwister: (state, action) => {
			state.twister = action.payload;
		},
		setM4i: (state, action) => {
			const { twister, m4i } = action.payload;
			return { ...state, m4i, twister };
		},
		removeUser: state => {
			return initialState;
		},
	},
});

// Action creators are generated for each case reducer function
export const { set, setTwister, setM4i, removeUser } = userSlice.actions;

export default userSlice.reducer;
