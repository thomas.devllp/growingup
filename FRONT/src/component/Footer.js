import React from 'react';

const Footer = () => {
	return (
		<footer>
			<p>© 2023 GrowingUP</p>
			<div>
				<p>Suivez-nous sur les réseaux :</p>
				<div className="facebook" />
				<div className="instagram" />
				<div className="linkedin" />
				<div className="twitter" />
			</div>
			<p>Hôte: <a href='https://www.vanaprincipia.fr/'>Vanaprincipia.fr</a></p>
		</footer>
	);
};

export default Footer;
