import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => {
	return (
		<nav className="headerContainer">
			<Link to={`/`}>
				<h2>
					Growing<span>UP</span>
					<span>.</span>
				</h2>
			</Link>
			<ul>
				<li>
					<Link to={`/add`}>Ajoutez vos informations</Link>
				</li>
				<li>
					<Link to={`/doc`}>Doc API</Link>
				</li>
			</ul>
		</nav>
	);
};

export default Nav;
