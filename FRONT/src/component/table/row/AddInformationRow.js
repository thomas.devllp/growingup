import React, { useEffect, useReducer, useState } from 'react';
import useActions from '../../../hooks/useActions';
import useFetch from '../../../hooks/useFetch';
import formReducer, {
	setFormAction,
	setValueAction,
} from '../../../reducers/state/formReducer';

const AddInformationRow = ({ handleAdd }) => {
	const [focus, setFocus] = useState();
	const [information, dispatchState] = useReducer(formReducer, {});
	const [setValue, resetValue] = useActions(dispatchState, [
		setValueAction,
		setFormAction,
	]);

	const handleChange = ({ target }) => {
		setValue(target.name, target.value);
	};

	const handleCategory = ({ target }) => {
		setValue('category', { name: target.value });
	};

	const handleUnity = ({ target }) => {
		setValue('unity', { name: target.value });
	};

	const handleAddController = () => {
		handleAdd(information);
		resetValue({});
	};

	return (
		<tr>
			<td className="" onClick={e => setFocus('category')}>
				<InputSuggestion
					onChange={handleCategory}
					value={information.category?.name}
					name="type"
					placeholder="ex: Taille, Famille"
					entity="category"
					focus={focus === 'category'}
				/>
			</td>
			<td>
				<input
					type="text"
					name="value"
					placeholder="22"
					onChange={handleChange}
					value={information.value || ''}
				/>
			</td>
			<td onClick={e => setFocus('unity')}>
				<InputSuggestion
					onChange={handleUnity}
					value={information.unity?.name}
					placeholder="cm"
					name="unity"
					entity="unity"
					focus={focus === 'unity'}
				/>
			</td>
			<td className="button_form_cell" onClick={handleAddController}>
				<div className="button">Ajouter</div>
			</td>
		</tr>
	);
};

const InputSuggestion = ({
	name,
	placeholder,
	onChange,
	value,
	entity,
	focus = false,
}) => {
	const [result, load] = useFetch(false);

	useEffect(() => {
		document.addEventListener('keydown', onKeyDown);
		return () => {
			document.removeEventListener('keydown', onKeyDown);
		};
	}, [result, focus]);

	const onKeyDown = e => {
		if (!focus) return;
		if (e.keyCode !== 9) return;
		e.preventDefault();
		if (result?.search) {
			onChange({ target: { value: result.search } });
		}
	};

	useEffect(() => {
		if (value) {
			load({
				url: `${entity}/suggestion?search=${value}`,
			});
		}
	}, [value]);

	return (
		<div className="group input suggestion">
			{value && result?.success && (
				<div className="input suggestion">
					<input disabled type="text" name="type" value={result.search || ''} />
				</div>
			)}
			<input
				type="text"
				onChange={onChange}
				value={value || ''}
				className="main-suggestion"
				name={name}
				placeholder={placeholder}
			/>
		</div>
	);
};

export default AddInformationRow;
