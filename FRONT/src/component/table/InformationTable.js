import React, { useState } from 'react';
import AddInformationRow from './row/AddInformationRow';
import { v4 as uuid } from 'uuid';

const InformationTable = ({ handleAdd, informations, handleDelete }) => {
	return (
		<table className="table">
			<thead>
				<tr>
					<th>Type</th>
					<th>Valeur</th>
					<th>Unité</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<AddInformationRow handleAdd={handleAdd} />
				{informations.map((information, index) => (
					<tr key={uuid()}>
						<td>{information.category.name}</td>
						<td>{information.value}</td>
						<td>{information.unity.name}</td>
						<td className="button_form_cell">
							<div className="button" onClick={() => handleDelete(index)}>
								Supprimer
							</div>
						</td>
					</tr>
				))}
			</tbody>
		</table>
	);
};

export default InformationTable;
