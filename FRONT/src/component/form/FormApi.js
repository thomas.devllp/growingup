import React from 'react';

const FormApi = ({ vehicules = [], checked, setChecked }) => {
	const handleChange = id => {
		if (checked[id]) {
			setChecked({ ...checked, [id]: false });
		} else {
			setChecked({ ...checked, [id]: true });
		}
	};

	return (
		<div className='vehicule_box'>
			{vehicules.map(checkbox => {
				return (
					<div key={checkbox.id} className="vehicule_item">
						<label>
							{checkbox.emoji.main} {checkbox.name}
						</label>
						<input
							type="checkbox"
							onChange={() => handleChange(checkbox.id)}
							checked={!!checked[checkbox.id]}
						/>
					</div>
				);
			})}
		</div>
	);
};

export default FormApi;
