import React from 'react';
import FormSelect from './FormSelect';

const FormDouble = ({
	bigTitleLeft,
	bigTitleRight,
	labelRight,
	labelLeft,
	value,
	handleChange,
	options,
	propertySelect = 'name',
}) => {
	const handleChangeController = ({ target }) => {
		handleChange(target, true);
	};

	return (
		<div className="form-content">
			<div className="form">
				<h1>{bigTitleLeft}</h1>
				<label className="form-label">
					{labelLeft}
					<FormSelect
						handleCallBack={handleChange}
						options={options}
						property={propertySelect}
					/>
				</label>
			</div>
			<div className="form form-insert">
				<h1>{bigTitleRight}</h1>
				<label className="form-label">
					{labelRight}
					<input
						type="text"
						name="value"
						className="form-input"
						onChange={handleChangeController}
						value={value || ''}
					/>
				</label>
			</div>
		</div>
	);
};

export default FormDouble;
