import React from 'react';

const FormSelect = ({ handleCallBack, options, property }) => {
	const handleChange = ({ target }) => {
		const { value } = target;
		handleCallBack(options[value]);
	};

	return (
		<select name="flowers" className="form-select" onChange={handleChange}>
			<option type="hidden">Choisissez une option</option>
			{options.map((option, index) => (
				<option value={index} key={option.id}>
					{option[property]}
				</option>
			))}
		</select>
	);
};

export default FormSelect;
