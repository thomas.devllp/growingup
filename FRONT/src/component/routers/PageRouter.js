// modules
import React from 'react';
import { Routes, Route } from 'react-router-dom';
import PageApi from '../pages/PageApi';
import Home from '../pages/Home';
// components
import Page404 from '../pages/Page404';
import PageSuggestion from '../pages/PageSuggestion';
import PageDoc from '../pages/PageDoc';
import PageForm from '../pages/PageForm';

function PageRouter() {
	return (
		<Routes>
			<Route path="/404" element={<Page404 />} />
			<Route path="/">
				<Route path="/" element={<Home />} />
				<Route path="/add" element={<PageForm />} />
				<Route path="/carbon" element={<PageApi />} />
				<Route path="/suggestion" element={<PageSuggestion />} />
				<Route path="/doc" element={<PageDoc />} />
			</Route>
		</Routes>
	);
}

export default PageRouter;
