import React, { useEffect, useState } from 'react';
import { objectToGETparams, roundFloat } from '../../functions';
import useFetch from '../../hooks/useFetch';
import FormApi from '../form/FormApi';
import { Link } from 'react-router-dom';
import Nav from '../Nav';
import Footer from '../Footer';

const ApiCarbon = ({}) => {
	const [result, load, loading] = useFetch();
	const [apiCarbon, setApiCarbon] = useState([]);
	const [value, setValue] = useState(1);
	const [checked, setChecked] = useState({});

	useEffect(() => {
		handleLoad();
	}, []);

	useEffect(() => {
		if (result) {
			if (result.success) {
				setApiCarbon(result.apiCarbon);
			}
		}
	}, [result]);

	const handleLoad = () => {
		load({
			url: 'carbon',
		});
	};

	const handleChangeCarbon = ({ target }) => {
		setValue(target.value);
	};

	const filtered = apiCarbon.filter(vehicule => !!checked[vehicule.id]);

	const resultCo2 = roundFloat(
		(filtered.reduce((acc, vehicule) => {
			return acc + vehicule.emissions.kgco2e;
		}, 0) *
			value) /
			filtered.length || 0
	);

	return (
		<div className="principalBox">
			<header>
				<Nav />
			</header>
			<main className="api-main">
				<Link to={`/`} className="button-retour">
					Retour à l'accueil
				</Link>
				<div className="container-api">
					<p>
						Calculez votre empreinte carbone grâce à notre APICarbon.
						<br />
						Obtenez une estimation en fonction de vos trajets ainsi que du type de
						véhicule utilisé.
					</p>
					<h1>ApiCarbon</h1>
					<div className="api-box">
						<div className="header-api">
							<p>1 - Entrez votre nombre de kilomètres dans le mois</p>
							<input type="number" onChange={handleChangeCarbon} placeholder="1"></input>
						</div>
						<div className="box-api">
							<p>2 - Choisissez vos moyens de transport</p>
							<form className="form_vehicule">
								<FormApi
									vehicules={apiCarbon}
									checked={checked}
									setChecked={setChecked}
								/>
							</form>
						</div>
						{/* <div>{listVehicule} </div> */}
						<div className="result-api">
							<p>En moyenne vous consommez : </p>
							<p>{resultCo2} kgCO2e</p>
							{/* <button onClick={handleSendApiValue}>Envoyer</button> */}
							<Link
								className="button"
								to={'/suggestion' + objectToGETparams({ resultCo2 })}
							>
								Envoyer
							</Link>
						</div>
						<div>
							{/* {floralArrangement.map((item, index) => (
						<div key={item.id}></div>
					))} */}
						</div>
					</div>
				</div>
			</main>
			<Footer />
		</div>
	);
};

export default ApiCarbon;
