import { Link } from 'react-router-dom';
import Footer from '../Footer';
import Nav from '../Nav';

const PageDoc = () => {
	return (
		<div className="principalBox">
			<header>
				<Nav />
			</header>
			<main className="home-main">
				<div className="doc-img"></div>
			</main>
			<Footer />
		</div>
	);
};

export default PageDoc;
