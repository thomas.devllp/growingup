import React, { useEffect, useReducer, useState } from 'react';
import { Link } from 'react-router-dom';
import { POST } from '../../constants/methods';
import useGetFlowers from '../../hooks/entities/useGetFlowers';
import useActions from '../../hooks/useActions';
import useFetch from '../../hooks/useFetch';
import useLocalStorage from '../../hooks/useLocalStorage';
import formReducer, {
	setFormAction,
	setValueAction,
} from '../../reducers/state/formReducer';
import Footer from '../Footer';
import FormDouble from '../form/FormDouble';
import Nav from '../Nav';
import InformationTable from '../table/InformationTable';

const defaultValue = {
	informations: [],
};

const PageForm = ({}) => {
	const { getStoredItem, setItemToStorage } = useLocalStorage();
	const [flowers, loadingFlowers] = useGetFlowers();
	const [result, load, loading] = useFetch();
	const [groups, dispatchState] = useReducer(
		formReducer,
		getStoredItem('groups') || defaultValue
	);
	const [setValue, resetValue] = useActions(dispatchState, [
		setValueAction,
		setFormAction,
	]);
	const [success, setSuccess] = useState(false);

	useEffect(() => {
		if (result) {
			if (result.success) {
				setItemToStorage(defaultValue, 'groups');
				resetValue(defaultValue);
				setSuccess(true);
			}
		}
	}, [result]);

	useEffect(() => {
		setItemToStorage(groups, 'groups');
	}, [groups]);

	const handleLoad = e => {
		e.preventDefault();
		load({
			url: 'information/create',
			method: POST,
			body: groups,
		});
	};

	const handleChangeFlower = (target, newValue = false) => {
		if (newValue) {
			setValue('flower', { name: target.value });
		} else {
			setValue('flower', target);
		}
	};

	const handleAddInformation = information => {
		const newInformations = groups.informations.slice();
		newInformations.push(information);
		setValue('informations', newInformations);
	};

	const handleDelete = index => {
		const newInformations = groups.informations.slice();
		newInformations.splice(index, 1);
		setValue('informations', newInformations);
		setItemToStorage(groups, 'groups');
	};

	if (success) {
		return (
			<div className="principalBox success">
				<div>
					<p>
						Growing Up vous remercie pour votre participation, vos données on été
						enregistrer et seront analyser par notre équipe
					</p>
					<Link to={`/`}>Retour à l'accueil</Link>
				</div>
			</div>
		);
	}

	return (
		<div className="principalBox">
			<header>
				<Nav />
			</header>
			<Link to={`/`} className="button-retour">
				Retour à l'accueil
			</Link>
			<main className="formFlower-main">
				<h2>Ajoutez vos informations</h2>

				<form className="formFlower">
					<div className="formDouble">
						<FormDouble
							bigTitleLeft={
								<>
									Choisissez
									<br /> vos informations
								</>
							}
							bigTitleRight={
								<>
									Rentrez
									<br /> vos informations
								</>
							}
							labelLeft="Choisisez votre plante"
							labelRight="Ajoutez une plante"
							handleChange={handleChangeFlower}
							options={flowers}
							value={groups.flower?.name}
						/>
					</div>
					<InformationTable
						handleAdd={handleAddInformation}
						informations={groups.informations}
						handleDelete={handleDelete}
					/>
					<div>
						<div className="button-submit" onClick={handleLoad}>
							Envoyez
						</div>
					</div>
				</form>
			</main>
			<Footer />
		</div>
	);
};

export default PageForm;
