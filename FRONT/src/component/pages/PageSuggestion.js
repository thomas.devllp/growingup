import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import useFetch from '../../hooks/useFetch';
import useQuery from '../../hooks/useQuery';
import Footer from '../Footer';
import Nav from '../Nav';

const PageSuggestion = () => {
	const query = useQuery();
	const [result, load, loading] = useFetch();
	const [floralArrangements, setFloralArrangements] = useState([]);
	const [ids, setIds] = useState([]);

	useEffect(() => {
		handleSendApiValue();
	}, []);

	useEffect(() => {
		if (result) {
			if (result.success) {
				setFloralArrangements(result.floralArrangements);
				setIds([...ids, ...result.floralArrangements.map(fa => fa.id)]);
			} else {
				setFloralArrangements([]);
			}
		}
	}, [result]);

	const handleSendApiValue = () => {
		load({
			url: 'carbon',
			method: 'POST',
			body: {
				value: query.resultCo2,
				ids,
			},
		});
	};

	return (
		<div className="principalBox">
			<header>
				<Nav />
			</header>
			<div className='suggestions'>
				{floralArrangements.length > 0 ? (
					<>
						{floralArrangements.map(floralArrangement => (
							<div className="block-flower" key={floralArrangement.id}>
								<div className='title'>Proposition des espèces à planter</div>
								<div className='list'>
									{floralArrangement.arrangements.map(arrangement => (
										<div key={arrangement.id} className='flower'>
											<div className='flower-name'>{arrangement.flower.name}</div>
											<img src={`./${arrangement.flower.name}.png`} alt="" />
											<div className='quantity'><span>{arrangement.quantity}</span></div>
										</div>
									))}
								</div>
							</div>
						))}
						<div className="next">
							<button className="button" onClick={handleSendApiValue}>
								Autre proposition
							</button>
						</div>
					</>
				) : (
					<div>
						{loading ? (
							<></>
						) : (
							<div className='no-flower'>
								Aucun résultat, Growing mets a jour regulierement de nouvelles
								propositions

								<button className='btn-back'>
									<Link to={`/`}>Retour à l'accueil</Link>
								</button>
							</div>
						)}
					</div>
				)}
			</div>
			<Footer />
		</div>
	);
};

export default PageSuggestion;
