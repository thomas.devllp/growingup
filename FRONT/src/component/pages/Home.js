import { Link } from 'react-router-dom';
import Footer from '../Footer';
import Nav from '../Nav';

const Home = () => {
	return (
		<div className="principalBox">
			<header>
				<Nav />
			</header>
			<main className="home-main">
				<div className="logo-img" />
				<div>
					<h3>Bienvenue !</h3>
					<hr />
					<p>
						Growing Up est née d’une volonté de savoir ce que vous produisez comme
						empreinte carbone annuelle.
					</p>
					<p>
						Votre mobilité, votre alimentation, vos dépenses pour votre domicile, vos
						loisirs, toutes ces actions ont un impact sur la planète.
					</p>
					<p>Aujourd’hui Growing Up vous permet de savoir et d’agir en conséquence.</p>
					<Link to={`/carbon`}>Calculer son empreinte carbone</Link>
				</div>
			</main>
			<Footer />
		</div>
	);
};

export default Home;
