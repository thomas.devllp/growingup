// modules
import React from 'react';
import Footer from '../Footer';
import Nav from '../Nav';

const Page404 = () => (
	<div>
		<header>
			<Nav />
		</header>
		<p>La page que vous recherchez n'existe pas.</p>
		<Footer />
	</div>
);
export default Page404;
