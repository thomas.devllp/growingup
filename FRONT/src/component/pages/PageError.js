// modules
import React from 'react';
import Footer from '../Footer';
import Nav from '../Nav';
// components

const PageError = ({ clearError }) => {
	return (
		<div>
			<header>
				<Nav />
			</header>
			<p>
				Nous sommes désolé pour le désagrément, une erreur est venue corrompre
				l'interface.
				<br /> Nous faisons notre maximum pour la corriger au plus vite.
			</p>
			<Footer />
		</div>
	);
};
export default PageError;
