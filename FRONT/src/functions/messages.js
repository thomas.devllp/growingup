/**
 * Transform array of errors object to message object with fieldName keys
 * @param {array} errors
 * @return {object}
 */
export function buildFieldsMessage(errors) {
	const message = {};
	errors.forEach(error => (message[error.field] = error.message));
	return message;
}

/**
 * Parse fieldname.attribute
 * @param {object} messages
 * @param {string} fieldName
 * @return {object}
 */
export function parseResponseMessage(messages, fieldName) {
	const length = fieldName.length;
	const parsedMessages = {};
	Object.entries(messages).forEach(([key, value]) => {
		if (key.slice(0, length) === fieldName) {
			parsedMessages[key.slice(length + 1)] = value; // +1 for "."
		}
	});
	return parsedMessages;
}
/**
 * Parse fieldname[index] || fieldname[index].attribute
 * @param {object} messages
 * @param {string} fieldName
 * @return {array}
 */
export function parseResponseMessageOfArray(messages, fieldName) {
	const length = fieldName.length;
	const parsedMessages = [];
	Object.entries(messages).forEach(([key, value]) => {
		if (key.slice(0, length) === fieldName) {
			let i = length + 1; // +1 for "["
			let index = '';
			// If there is more than 9 index in array
			while (key.charAt(i) !== ']' && i < key.length) {
				index += key.charAt(i);
				i++;
			}
			if (key.charAt(i + 1) === '.') {
				let attrName = key.slice(i + 2); // +2 for "]."
				if (parsedMessages[index]) {
					parsedMessages[index][attrName] = value;
				} else {
					parsedMessages[index] = { [attrName]: value };
				}
			} else {
				parsedMessages[index] = value;
			}
		}
	});
	return parsedMessages;
}
