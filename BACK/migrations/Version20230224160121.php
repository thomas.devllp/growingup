<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230224160121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE unity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, is_confirmed TINYINT(1) DEFAULT 0 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE information ADD unity_id INT NOT NULL');
        $this->addSql('ALTER TABLE information ADD CONSTRAINT FK_29791883F6859C8C FOREIGN KEY (unity_id) REFERENCES unity (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_29791883F6859C8C ON information (unity_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE information DROP FOREIGN KEY FK_29791883F6859C8C');
        $this->addSql('DROP TABLE unity');
        $this->addSql('DROP INDEX IDX_29791883F6859C8C ON information');
        $this->addSql('ALTER TABLE information DROP unity_id');
    }
}
