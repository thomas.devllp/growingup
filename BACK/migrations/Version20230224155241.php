<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230224155241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flower ADD is_confirmed TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE information ADD CONSTRAINT FK_2979188312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE information ADD CONSTRAINT FK_2979188379414B35 FOREIGN KEY (information_group_id) REFERENCES information_group (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2979188312469DE2 ON information (category_id)');
        $this->addSql('CREATE INDEX IDX_2979188379414B35 ON information (information_group_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flower DROP is_confirmed');
        $this->addSql('ALTER TABLE information DROP FOREIGN KEY FK_2979188312469DE2');
        $this->addSql('ALTER TABLE information DROP FOREIGN KEY FK_2979188379414B35');
        $this->addSql('DROP INDEX IDX_2979188312469DE2 ON information');
        $this->addSql('DROP INDEX IDX_2979188379414B35 ON information');
    }
}
