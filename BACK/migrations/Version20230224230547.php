<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230224230547 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE arrangement (id INT AUTO_INCREMENT NOT NULL, flower_id INT DEFAULT NULL, floral_arrangement_id INT DEFAULT NULL, quantity INT NOT NULL, INDEX IDX_7E99C3B92C09D409 (flower_id), INDEX IDX_7E99C3B968CDA57D (floral_arrangement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE floral_arrangement (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE arrangement ADD CONSTRAINT FK_7E99C3B92C09D409 FOREIGN KEY (flower_id) REFERENCES flower (id)');
        $this->addSql('ALTER TABLE arrangement ADD CONSTRAINT FK_7E99C3B968CDA57D FOREIGN KEY (floral_arrangement_id) REFERENCES floral_arrangement (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE arrangement DROP FOREIGN KEY FK_7E99C3B92C09D409');
        $this->addSql('ALTER TABLE arrangement DROP FOREIGN KEY FK_7E99C3B968CDA57D');
        $this->addSql('DROP TABLE arrangement');
        $this->addSql('DROP TABLE floral_arrangement');
    }
}
