<?php

namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class NullNormalizer
 * @package App\Normalizer
 */
class NullNormalizer extends ObjectNormalizer
{
    /** {@inheritdoc} */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return false;
    }

    /** {@inheritdoc} */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && in_array('', $data);
    }

    /** {@inheritdoc} */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $reflectionClass = new \ReflectionClass($type);
        foreach ($data as $key => $value) {
            // Replace '' by null on number columns
            if ($value === '' && $reflectionClass->hasProperty($key)) {
                $annotations = explode(
                    PHP_EOL,
                    $reflectionClass->getProperty($key)->getDocComment()
                );
                foreach ($annotations as $annotation) {
                    if (preg_match(
                        '/@ORM\\\\Column\(type="(float|int|double|long|datetime)"/',
                        $annotation
                    )) {
                        $data[$key] = null;
                        break;
                    };
                }
            }
        }
        return parent::denormalize($data, $type, $format, $context);
    }
}
