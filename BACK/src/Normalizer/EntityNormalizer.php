<?php

namespace App\Normalizer;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EntityNormalizer
 * @package App\Normalizer
 */
class EntityNormalizer extends ObjectNormalizer
{
    protected EntityManagerInterface $em;
    protected ?Request $request = null;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null,
        ?ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null,
        ?callable $objectClassResolver = null,
        array $defaultContext = []
    ) {
        parent::__construct(
            $classMetadataFactory,
            $nameConverter,
            $propertyAccessor,
            $propertyTypeExtractor,
            $classDiscriminatorResolver,
            $objectClassResolver,
            $defaultContext
        );

        $this->em      = $em;
        $this->request = $requestStack->getCurrentRequest();
    }

    /** {@inheritdoc} */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return false;
    }

    /** {@inheritdoc} */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        $condition = strpos($type, 'App\\Entity\\') === 0
            && (is_numeric($data)
                || is_string($data)
                || (is_array($data)
                    && array_key_exists('id', $data)
                    // && count($data) === 1
                )
            );

        return $condition;
    }

    /** {@inheritdoc} */
    public function denormalize($data, $class, string $format = null, array $context = []): mixed
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            $data = $data['id'];
        }

        return $this->em->find($class, $data);
    }
}
