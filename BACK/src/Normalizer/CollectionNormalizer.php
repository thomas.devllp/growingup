<?php

namespace App\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class CollectionNormalizer
 * @package App\Normalizer
 */
class CollectionNormalizer extends ObjectNormalizer
{
    /** {@inheritdoc} */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return false;
    }

    /** {@inheritdoc} */
    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof Collection;
    }

    /** {@inheritdoc} */
    public function normalize(mixed $collection, ?string $format = null, array $context = []): mixed
    {
        $normalized = [];
        $collection = $collection->toArray();
        foreach ($collection as $val) {
            $normalized[] = $this->serializer->normalize($val, $format, $context);
        }

        return $normalized;
    }
}
