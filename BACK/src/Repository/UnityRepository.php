<?php

namespace App\Repository;

use App\Entity\Unity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Unity>
 *
 * @method Unity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Unity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Unity[]    findAll()
 * @method Unity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Unity::class);
    }

    public function save(Unity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Unity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByName(string $name): ?Unity
    {
        return $this->createQueryBuilder('u')
            ->where('u.name LIKE :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findSuggestion(string $search)
    {
        return $this->createQueryBuilder('u')
            ->select('u.name as search')
            ->where('u.name LIKE :search')
            ->andWhere('u.isConfirmed = 1')
            ->setParameter('search',  $search . '%')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
