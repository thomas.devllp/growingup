<?php

namespace App\Repository;

use App\Entity\InformationGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InformationGroup>
 *
 * @method InformationGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformationGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformationGroup[]    findAll()
 * @method InformationGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationGroup::class);
    }

    public function save(InformationGroup $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(InformationGroup $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
