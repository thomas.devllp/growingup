<?php

namespace App\Repository;

use App\Entity\Flower;
use App\Entity\Information;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Information>
 *
 * @method Information|null find($id, $lockMode = null, $lockVersion = null)
 * @method Information|null findOneBy(array $criteria, array $orderBy = null)
 * @method Information[]    findAll()
 * @method Information[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Information::class);
    }

    public function save(Information $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Information $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getFlowerAbsorbtion(Flower $flower)
    {
        return $this->createQueryBuilder('i')
            ->leftJoin('i.informationGroup','ig')
            ->leftJoin('i.unity', 'u')
            ->where('ig.flower = :id')->setParameter('id', $flower->getId())
            ->andWhere('u.id = :id')->setParameter('id', '1')
            ->andWhere('i.category = :id')->setParameter('id', 1)
            ->getQuery()
            ->getResult();
    }

}
