<?php

namespace App\Repository;

use App\Entity\Arrangement;
use App\Entity\FloralArrangement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FloralArrangement>
 *
 * @method FloralArrangement|null find($id, $lockMode = null, $lockVersion = null)
 * @method FloralArrangement|null findOneBy(array $criteria, array $orderBy = null)
 * @method FloralArrangement[]    findAll()
 * @method FloralArrangement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FloralArrangementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FloralArrangement::class);
    }

    public function save(FloralArrangement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(FloralArrangement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBestArrangements($consommation, $ids)
    {
        $consoDQL = $this->getConsommationSubquery('SUM(a_ss.consommation)', 'a_ss');
        $consoTriDQL = $this->getConsommationSubquery('ABS(:conso - SUM(a_ss2.consommation))', 'a_ss2');

        $query =  $this->createQueryBuilder('fa')
            ->select(['fa'])
            ->addSelect("($consoDQL) as totalConsommation")
            ->addSelect("($consoTriDQL) as triConsommation")
            ->orderBy('triConsommation', 'ASC')
            ->setParameter('conso', $consommation);

        if (!empty($ids)) {
            $query->add('where', $query->expr()->notIn('fa.id', $ids));
        }

        $result = $query
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $result;
    }

    private function getConsommationSubquery($select, $s)
    {
        return $this
            ->getEntityManager()
            ->getRepository(Arrangement::class)
            ->createQueryBuilder("$s")
            ->select($select)
            ->where("$s.floralArrangement = fa.id")
            ->getQuery()
            ->getDQL();
    }

    //    public function getAllArrangementsFlowers($id){
    //        dd($this->createQueryBuilder('a')
    //            ->where('a.id = :id')->setParameter('id', $id)
    //            ->getQuery()
    //            ->getArrayResult());
    //        return $this->createQueryBuilder('a')
    //            ->getQuery()
    //            ->getResult();
    //    }



    //    /**
    //     * @return FloralArrangement[] Returns an array of FloralArrangement objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('f.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?FloralArrangement
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
