<?php

namespace App\Repository;

use App\Entity\Flower;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Flower>
 *
 * @method Flower|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flower|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flower[]    findAll()
 * @method Flower[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlowerRepository extends ServiceEntityRepository
{
    private $filter = null;
    private $filterValue = null;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flower::class);
    }

    public function save(Flower $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Flower $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByName(string $name): ?Flower
    {
        return $this->createQueryBuilder('f')
            ->where('f.name LIKE :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findWithAbsorption()
    {
        $query = $this->createQueryBuilder('f')
            ->select(['f', 'i'])
            ->innerJoin('f.informationGroups', 'ig')
            ->innerJoin('ig.informations', 'i')
            ->innerJoin('i.category', 'c', 'WITH', "c.name LIKE 'Absorption CO2'")
            ->getQuery()
            ->getResult();

        return $query;
    }

    public function findOneByFilter(): ?Flower
    {
        $query = $this->createQueryBuilder('f')
            ->select(['f', 'ig', 'i', 'c'])
            ->innerJoin('f.informationGroups', 'ig')
            ->innerJoin('ig.informations', 'i')
            ->innerJoin('i.category', 'c');

        $this->filter($query);

        return $query->getQuery()
            ->getOneOrNullResult();
    }

    private function filter($query)
    {
        switch ($this->filter) {
            case 'absorption':
                $query
                    ->andWhere("f.name LIKE :name")
                    ->andWhere("c.name LIKE 'Absorption CO2'")
                    ->setParameter('name', $this->filterValue);;
                break;
            default:
                $query->andWhere('f.id = :id')
                    ->setParameter('id', $this->filterValue);
                break;
        }
    }

    public function setFilter(string $filter, ?string $value = null)
    {
        $this->filter = $filter;
        $this->filterValue = $value;
    }
}
