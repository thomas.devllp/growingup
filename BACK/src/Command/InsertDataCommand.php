<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\Flower;
use App\Entity\Information;
use App\Entity\InformationGroup;
use App\Entity\Unity;
use App\Repository\EnterpriseRepository;
use App\Service\DataManager\ChargeTypeData;
use App\Service\DataManager\InformationGroupData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:insert-data',
    description: 'Create default data'
)]
class InsertDataCommand extends Command
{

    public function __construct(
        protected EntityManagerInterface $em,
        protected ValidatorInterface $validator,
        private InformationGroupData $data
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setHelp(self::getDefaultDescription());
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $application = $this->getApplication();
        $application->setAutoExit(false);

        $output->writeln([
            '===========================================================',
            '*** Insert default data ***',
            '===========================================================',
            '',
        ]);


        $counter = 0;
        foreach ($this->getCategories() as $value) {
            $category = new Category();
            $category->setName($value);
            $category->setIsConfirmed(true);

            $this->em->persist($category);
            $counter++;
        }

        foreach ($this->getFlowers() as $value) {
            $category = new Flower();
            $category->setName($value);
            $category->setIsConfirmed(true);

            $this->em->persist($category);
            $counter++;
        }

        foreach ($this->getUnity() as $value) {
            $category = new Unity();
            $category->setName($value);
            $category->setIsConfirmed(true);

            $this->em->persist($category);
            $counter++;
        }

        $this->em->flush();

        $io->success("$counter data ont été créées.");

        return Command::SUCCESS;
    }


    private function getCategories()
    {
        return [
            'Absorption CO2',
            'Taille',
            'Famille',
            'Mois de floraison',
            'Polluants atmosphériques',
            'Particules',
            'Régulation du climat local',
            'Risque allergique',
            'Contraintes physiques',
            'Other',
            'Surface'
        ];
    }

    private function getFlowers()
    {
        return [
            'Rose',
            'Tulipe',
            'Micocoulier'
        ];
    }

    private function getUnity()
    {
        return [
            'kCO2/an',
            'mm',
            'cm',
            'm',
        ];
    }
}
