<?php

namespace App\Command;

use App\Entity\Arrangement;
use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\FloralArrangement;
use App\Entity\Flower;
use App\Entity\Information;
use App\Entity\InformationGroup;
use App\Entity\Unity;
use App\Repository\EnterpriseRepository;
use App\Repository\FlowerRepository;
use App\Service\DataManager\ChargeTypeData;
use App\Service\DataManager\InformationGroupData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:insert-arrangement',
    description: 'Create default data'
)]
class InsertArrangementCommand extends Command
{

    public function __construct(
        protected EntityManagerInterface $em,
        protected ValidatorInterface $validator,
        private InformationGroupData $data,
        private FlowerRepository $flowerRepository
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setHelp(self::getDefaultDescription());
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $application = $this->getApplication();
        $application->setAutoExit(false);

        $output->writeln([
            '===========================================================',
            '*** Insert default data ***',
            '===========================================================',
            '',
        ]);

        $counter = 0;

        $arrangements = [
            [
                'Tulipe' => 8,
                'Rose' => 10,
            ],
            [
                'Tulipe' => 5,
                'Rose' => 20,
            ],
            [
                'Tulipe' => 5,
                'Cactus' => 2
            ], [
                'Bambou' => 1,
                'Rose' => 10,
            ]


        ];

        foreach ($arrangements as $key => $value) {
            $fa = $this->Floral("Arrangement $key", $value);
            $this->em->persist($fa);
        }

        $counter++;

        $this->em->flush();

        $io->success("$counter data ont été créées.");

        return Command::SUCCESS;
    }

    private function Floral($name, $array)
    {
        $fa = new FloralArrangement();
        $fa->setName($name);

        foreach ($array as $key => $value) {
            $arrangement = new Arrangement();

            $this->flowerRepository->setFilter("absorption", $key);
            $flower = $this->flowerRepository->findOneByFilter();

            $arrangement->setFlower($flower);
            $arrangement->setQuantity($value);

            $absorption = 0;
            $infos = 0;
            foreach ($flower->getInformationGroups() as $groups) {
                foreach ($groups->getInformations() as $information) {
                    $infos++;
                    $absorption += $information->getValue();
                }
            }

            $arrangement->setConsommation(($absorption / $infos) * $arrangement->getQuantity());
            $fa->addArrangement($arrangement);
        }

        return $fa;
    }
}
