<?php

namespace App\Command;

use App\Entity\Arrangement;
use App\Entity\Category;
use App\Entity\ChargeType;
use App\Entity\FloralArrangement;
use App\Entity\Flower;
use App\Entity\Information;
use App\Entity\InformationGroup;
use App\Entity\Unity;
use App\Repository\EnterpriseRepository;
use App\Repository\FlowerRepository;
use App\Service\DataManager\ChargeTypeData;
use App\Service\DataManager\InformationGroupData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:insert-information',
    description: 'Create default data'
)]
class InsertInformationCommand extends Command
{

    public function __construct(
        protected EntityManagerInterface $em,
        protected ValidatorInterface $validator,
        private InformationGroupData $data,
        private FlowerRepository $flowerRepository
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setHelp(self::getDefaultDescription());
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $application = $this->getApplication();
        $application->setAutoExit(false);

        $output->writeln([
            '===========================================================',
            '*** Insert default data ***',
            '===========================================================',
            '',
        ]);

        $counter = 0;

        foreach ($this->getGroups() as $value) {

            $this->data->findOrCreate($value);

            $this->em->persist($value);
            $counter++;
        }

        $this->em->flush();


        $counter++;

        $this->em->flush();

        $io->success("$counter data ont été créées.");

        return Command::SUCCESS;
    }

    //2kil CO2/an
    //Selon les recherches, une plante moyenne absorbe environ 10 milligrammes de CO2 par heure pour chaque centimètre carré de feuillage. Cela signifie qu'une plante avec un feuillage de 1 mètre carré absorberait environ 1 kilogramme de CO2 en une année entière.
    //Les arbustes : Les arbustes, qui ont des tiges ligneuses et ramifiées, sont également efficaces pour absorber le CO2. Ils peuvent absorber entre 2,5 et 6 kilogrammes de CO2 par an, en fonction de leur taille et de leur espèce.
    private function getGroups()
    {
        $result = [];
        $result[] = $this->addInformationGroup('Algues')
            ->addNewInformation('Absorption CO2', 1.8, 'kCO2/an')
            ->addNewInformation('Surface', 1, 'm²');


        $result[] = $this->addInformationGroup('ARBRE DE JUDEE')
            ->addNewInformation('Absorption CO2', 22 * 0.6, 'kCO2/an')
            ->addNewInformation('Taille', 10, 'm');

        $result[] = $this->addInformationGroup('Bambou')
            ->addNewInformation('Absorption CO2', 12, 'tCO2/an')
            ->addNewInformation('Surface', 1, 'Hectare');

        $result[] = $this->addInformationGroup('Cactus')
            ->addNewInformation('Absorption CO2', 1, 'kCO2/an')
            ->addNewInformation('Taille', 0.5, 'm');

        $result[] = $this->addInformationGroup('Tulipe')
            ->addNewInformation('Absorption CO2', 0.1, 'kCO2/an')
            ->addNewInformation('Taille', 20, 'cm');

        $result[] = $this->addInformationGroup('Rose')
            ->addNewInformation('Absorption CO2', 0.2, 'kCO2/an')
            ->addNewInformation('Taille', 9, 'cm');

        return $result;
    }


    private function addInformationGroup(string $newFlower)
    {
        $flower = new Flower();
        $flower->setName($newFlower);

        $informationGroup = new InformationGroup();
        $informationGroup->setFlower($flower);

        return $informationGroup;
    }
}
