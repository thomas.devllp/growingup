<?php

namespace App\Entity;

use App\Repository\InformationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InformationRepository::class)]
class Information
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(['information:write', 'information:read'])]
    private $id = null;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: "informations", cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    #[Groups(['information:write', 'information:read'])]
    private ?Category $category = null;

    #[ORM\ManyToOne(targetEntity: InformationGroup::class, inversedBy: "informations", cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    private ?InformationGroup $informationGroup = null;

    #[ORM\Column(length: 255)]
    #[Groups(['information:write', 'information:read'])]
    private ?string $value = null;

    #[ORM\ManyToOne(targetEntity: Unity::class, inversedBy: "informations", cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    #[Groups(['information:write', 'information:read'])]
    private ?Unity $unity = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUnity(): ?Unity
    {
        return $this->unity;
    }

    public function setUnity(Unity $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    public function getInformationGroup(): ?InformationGroup
    {
        return $this->informationGroup;
    }

    public function setInformationGroup(InformationGroup $informationGroup): self
    {
        $this->informationGroup = $informationGroup;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
