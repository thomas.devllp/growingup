<?php

namespace App\Entity;

use App\Repository\FlowerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FlowerRepository::class)]
class Flower
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(['flower:read', 'test', 'informationGroup:write', 'floral_arrangement:read'])]
    private $id = null;

    #[ORM\Column(length: 180)]
    #[Groups(['flower:read', 'information:write', 'floral_arrangement:read'])]
    #[Assert\NotBlank(message: "Name est requis")]
    #[Assert\NotNull(message: "Name est requis")]
    protected ?string $name = null;

    #[ORM\OneToMany(targetEntity: InformationGroup::class, mappedBy: "flower", /* cascade: ['persist'] */)]
    private Collection $informationGroups;

    #[ORM\OneToMany(targetEntity: Arrangement::class, mappedBy: "flower", /* cascade: ['persist'] */)]
    private $arrangement;

    #[ORM\Column(type: "boolean", options: ["default" => false])]
    private bool $isConfirmed = false;

    public function __construct()
    {
        $this->informationGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsConfirmed(): bool
    {
        return $this->isConfirmed;
    }

    public function setIsConfirmed(bool $isConfirmed): self
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * @return Collection|InformationGroup[]
     */
    public function getInformationGroups(): Collection
    {
        return $this->informationGroups;
    }

    public function addInformationGroup(InformationGroup $informationGroup): self
    {
        if (!$this->informationGroups->contains($informationGroup)) {
            $this->informationGroups[] = $informationGroup;
            $informationGroup->setFlower($this);
        }
        return $this;
    }

    public function removeInformationGroup(InformationGroup $informationGroup): self
    {
        if ($this->informationGroups->contains($informationGroup)) {
            $this->informationGroups->removeElement($informationGroup);
        }

        return $this;
    }
}
