<?php

namespace App\Entity;

use App\Repository\FloralArrangementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FloralArrangementRepository::class)]
class FloralArrangement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(['category:read', 'information:write', 'floral_arrangement:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    #[Groups(['category:read', 'information:write', 'floral_arrangement:read'])]
    protected ?string $name = null;

    #[ORM\OneToMany(targetEntity: Arrangement::class, mappedBy: "floralArrangement",  cascade: ['persist'])]
    #[Groups(['floral_arrangement:read'])]
    private $arrangements;

    public function __construct()
    {
        $this->arrangements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Arrangement[]
     */
    public function getArrangements(): Collection
    {
        return $this->arrangements;
    }

    public function addArrangement(Arrangement $arrangement): self
    {
        if (!$this->arrangements->contains($arrangement)) {
            $this->arrangements[] = $arrangement;
            $arrangement->setFloralArrangement($this);
        }
        return $this;
    }

    public function removeArrangement(Arrangement $arrangement): self
    {
        if ($this->arrangements->contains($arrangement)) {
            $this->arrangements->removeElement($arrangement);
        }

        return $this;
    }
}
