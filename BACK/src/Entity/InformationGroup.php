<?php

namespace App\Entity;

use App\Repository\InformationGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InformationGroupRepository::class)]
class InformationGroup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(['information:write'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Flower::class, inversedBy: "informationGroups", cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    #[Groups(['information:write'])]
    private ?Flower $flower = null;

    #[ORM\OneToMany(targetEntity: Information::class, mappedBy: "informationGroup", cascade: ['persist'])]
    #[Groups(['information:write'])]
    private Collection $informations;

    public function __construct()
    {
        $this->informations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlower(): ?Flower
    {
        return $this->flower;
    }

    public function setFlower(Flower $flower): self
    {
        $this->flower = $flower;

        return $this;
    }

    /**
     * @return Collection|Information[]
     */
    public function getInformations(): Collection
    {
        return $this->informations;
    }

    public function addInformation(Information $information): self
    {
        if (!$this->informations->contains($information)) {
            $this->informations[] = $information;
            $information->setInformationGroup($this);
        }
        return $this;
    }

    public function removeInformation(Information $information): self
    {
        if ($this->informations->contains($information)) {
            $this->informations->removeElement($information);
        }

        return $this;
    }


    //User in command

    public function addNewInformation(string $newCategory, string $value, string $newUnity)
    {
        $information = new Information();

        $category = new Category();
        $category->setName($newCategory);
        $information->setCategory($category);


        $unity = new Unity();
        $unity->setName($newUnity);
        $information->setUnity($unity);

        $information->setValue($value);

        $this->addInformation($information);

        return $this;
    }
}
