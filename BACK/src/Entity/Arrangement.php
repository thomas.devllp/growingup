<?php

namespace App\Entity;

use App\Repository\ArrangementRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ArrangementRepository::class)]
class Arrangement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['floral_arrangement:read'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Flower::class, inversedBy: "arrangement", /* cascade: ['persist'] */)]
    #[Groups(['floral_arrangement:read'])]
    private $flower;

    #[ORM\ManyToOne(targetEntity: FloralArrangement::class, inversedBy: "arrangements", /* cascade: ['persist'] */)]
    private $floralArrangement;

    #[ORM\Column(length: 255)]
    #[Groups(['category:read', 'information:write', 'floral_arrangement:read'])]
    private ?int $quantity = null;

    #[ORM\Column(type: "float")]
    private ?float $consommation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFlower()
    {
        return $this->flower;
    }

    /**
     * @param mixed $flower
     */
    public function setFlower($flower): void
    {
        $this->flower = $flower;
    }

    /**
     * @return mixed
     */
    public function getFloralArrangement()
    {
        return $this->floralArrangement;
    }

    /**
     * @param mixed $floralArrangement
     */
    public function setFloralArrangement($floralArrangement): void
    {
        $this->floralArrangement = $floralArrangement;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }


    public function getConsommation(): ?float
    {
        return $this->consommation;
    }

    public function setConsommation(?float $consommation): void
    {
        $this->consommation = $consommation;
    }
}
