<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Information;
use App\Entity\InformationGroup;
use App\Repository\CategoryRepository;
use App\Repository\InformationRepository;
use App\Service\DataManager\InformationGroupData;
use App\Service\DataManager\Traits\MergeFindAll;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/information', name: 'information_')]
class InformationController extends AbstractController
{
    use MergeFindAll;

    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer,
        private InFormationRepository $repo
    ) {
    }

    #[Route(
        "",
        name: 'information_all',
        methods: 'GET',
    )]
    public function all()
    {
        $informations = $this->repo->findAll();

        return $this->json(
            [
                'information' =>
                $this->mergeFindAll($informations, ['groups' => 'information:read'])
            ],
            Response::HTTP_OK
        );
    }

    #[Route(
        path: '/create',
        name: 'information_create',
        methods: 'POST',
    )]
    public function create(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        InformationGroupData $informationData
    ) {
        $json = $request->getContent();

        try {
            /** @var InformationGroup $informationGroup */
            $informationGroup = $serializer->deserialize(
                $json,
                InformationGroup::class,
                'json',
                [
                    'groups' => ['information:write'],
                ]
            );
        } catch (\Throwable $e) {
            dump($e);
        }

        $informationData->findOrCreate($informationGroup);

        $em->persist($informationGroup);
        $em->flush();

        return $this->json(
            ['information' => $informationGroup],
            Response::HTTP_OK,
            [],
            ['groups' => 'information:write']
        );
    }
}
