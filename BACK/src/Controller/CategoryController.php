<?php

namespace App\Controller;

use App\Entity\Flower;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DataManager\Traits\MergeFindAll;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/category', name: 'category_')]
class CategoryController extends AbstractController
{
    use MergeFindAll;

    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer,
        private CategoryRepository $repo
    ) {
    }

    #[Route(
        "",
        name: 'category_all',
        methods: 'GET',
    )]
    public function all()
    {
        $categories = $this->repo->findAll();

        return $this->json(
            [
                'categories' =>
                $this->mergeFindAll($categories, ['groups' => 'category:read'])
            ],
            Response::HTTP_OK
        );
    }

    #[Route(
        "/suggestion",
        name: 'suggestion',
        methods: 'GET',
    )]
    public function suggestion(
        Request $request
    ) {
        $search = $request->query->get('search');

        $search = $this->repo->findSuggestion($search);

        return $this->json(
            $search,
            Response::HTTP_OK
        );
    }
}
