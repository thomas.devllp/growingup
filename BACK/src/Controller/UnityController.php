<?php

namespace App\Controller;

use App\Repository\UnityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DataManager\Traits\MergeFindAll;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/unity', name: 'unity_')]
class UnityController extends AbstractController
{
    use MergeFindAll;

    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer,
        private UnityRepository $repo
    ) {
    }

    #[Route(
        "/suggestion",
        name: 'suggestion',
        methods: 'GET',
    )]
    public function suggestion(
        Request $request
    ) {
        $search = $request->query->get('search');

        $search = $this->repo->findSuggestion($search);

        return $this->json(
            $search,
            Response::HTTP_OK
        );
    }
}
