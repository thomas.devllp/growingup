<?php

namespace App\Controller;

use App\Entity\FloralArrangement;
use App\Repository\FloralArrangementRepository;
use App\Repository\FlowerRepository;
use App\Service\DataManager\CalculateAbsorbtion;
use App\Service\DataManager\Traits\MergeFindAll;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\TextUI\XmlConfiguration\Group;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/carbon', name: 'app_api_carbon')]
class ApiCarbonController extends AbstractController
{
    use MergeFindAll;

    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer,
    ) {
    }


    #[Route(
        "",
        name: 'carbon_get',
        methods: ['GET']
    )]
    public function index(): Response
    {
        $json = file_get_contents('./../storage/apiCarbon.json');
        $json = json_decode($json);
        return $this->json(
            ['apiCarbon' => $json],
            Response::HTTP_OK
        );
    }

    #[Route(
        "",
        name: 'carbon_form',
        methods: ['POST']
    )]
    public function create(
        Request $request,

        FloralArrangementRepository $faRepository,
    ): Response {
        $json = $request->getContent();
        $data = json_decode($json, true);

        //valueCarbon kgCO2/months
        $valueCarbon = $data['value'];

        $ids = $data['ids'];

        $fas = $faRepository->findBestArrangements($valueCarbon, $ids);

        return $this->json(
            ['floralArrangements' =>
            $this->mergeFindAll($fas, ['groups' => 'floral_arrangement:read'])],
            Response::HTTP_OK,
        );
    }
}
