<?php

namespace App\Controller;

use App\Entity\FloralArrangement;
use App\Service\DataManager\CalculateAbsorbtion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FloralArrangementController extends AbstractController
{
    #[Route('/floral/arrangement/{id}', name: 'app_floral_arrangement')]
    public function index(CalculateAbsorbtion $calculateAbsorbtion, FloralArrangement $floralArrangement): Response
    {
        $calculateAbsorbtion->calculate(115, $floralArrangement);
        dd($calculateAbsorbtion);
        return $this->render('floral_arrangement/index.html.twig', [
            'controller_name' => 'FloralArrangementController',
        ]);
    }
}
