<?php

namespace App\Controller;

use App\Entity\Flower;
use App\Repository\FlowerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DataManager\Traits\MergeFindAll;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/flower', name: 'flower_')]
class FlowerController extends AbstractController
{
    use MergeFindAll;

    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer,
        private FlowerRepository $repo
    ) {
    }

    #[Route(
        "",
        name: 'flower_all',
        methods: 'GET',
    )]
    public function all()
    {
        $flowers = $this->repo->findAll();

        return $this->json(
            [
                'flowers' =>
                $this->mergeFindAll($flowers, ['groups' => 'flower:read'])
            ],
            Response::HTTP_OK
        );
    }

    #[Route(
        "",
        name: 'flower_create',
        methods: 'POST',
    )]
    public function create()
    {
        return $this->json(
            ['message' => 'OK TEST'],
            Response::HTTP_OK
        );
    }
}
