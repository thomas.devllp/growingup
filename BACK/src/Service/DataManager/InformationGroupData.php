<?php

namespace App\Service\DataManager;

use App\Entity\InformationGroup;
use App\Repository\CategoryRepository;
use App\Repository\FlowerRepository;
use App\Repository\InformationRepository;
use App\Repository\UnityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class InformationGroupData extends DataManager
{
    /** @var InformationRepository $repository */
    protected $repository;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        private FlowerRepository $flowerRepository,
        private CategoryRepository $categoryRepository,
        private UnityRepository $unityRepository,
        private ConverterUnit $converterUnit
    ) {
        parent::__construct($em, $requestStack);
    }

    public function findOrCreate(InformationGroup $informationGroup)
    {
        if (!is_null($informationGroup->getFlower())) {
            $flower = $this->flowerRepository->findByName($informationGroup->getFlower()->getName());
            if (!is_null($flower)) {
                $informationGroup->setFlower($flower);
            }
        }

        foreach ($informationGroup->getInformations() as $information) {
            $category = $this->categoryRepository->findByName($information->getCategory()->getName());
            if (!is_null($category)) {
                $information->setCategory($category);
            }

            if (!is_null($information->getUnity())) {
                try {
                    $information = $this->converterUnit->convert($information);
                } catch (Exception $e) {
                    dump($e->getMessage());
                }
            }
        }
    }

    /** @required */
    public function setRepository()
    {
        $this->repository = $this->em->getRepository(InformationGroup::class);
    }
} {
}
