<?php

namespace App\Service\DataManager\Traits;

// Needs to add SerialierInterface $this->serializer in constructor
trait MergeFindAll
{
    public function mergeFindAll(array $datas, array $context = []): array
    {
        $return = [];

        foreach ($datas as $data) {
            if (!is_null($data)) {
                $return[] = $this->mergeOneFindAll($data, $context);
            }
        }
        return $return;
    }

    public function mergeOneFindAll($datas, array $context = []): array
    {
        $return = [];

        if (is_object($datas) && strpos(get_class($datas), 'App\\Entity\\') === 0) {
            $normalized_data = $this->serializer->normalize($datas, 'json', $context);
            return array_merge($return, $normalized_data);
        }

        foreach ($datas as $k_elt => $elt) {
            if (is_object($elt) && strpos(get_class($elt), 'App\\Entity\\') === 0) {
                $normalized_data = $this->serializer->normalize($elt, 'json', $context);
                $return = array_merge($return, $normalized_data);
            } else {
                $return[$k_elt] = $elt;
            }
        }

        return $return;
    }


    /**
     * Fonction probablement inutile si les array_merge de la classe agissent correctement
     *
     * @param array $return
     * @param array $array
     * @return array
     */
    public function arrayFusion(array $return, array $array): array
    {
        foreach ($array as $k => $v) {
            $return[$k] = $v;
        }
        return $return;
    }
}
