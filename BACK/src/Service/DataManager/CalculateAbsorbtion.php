<?php

namespace App\Service\DataManager;

use App\Entity\Arrangement;
use App\Entity\FloralArrangement;
use App\Entity\Flower;
use App\Entity\Information;
use App\Repository\ArrangementRepository;
use App\Repository\FloralArrangementRepository;
use App\Repository\InformationRepository;

class CalculateAbsorbtion
{
    /** @var ArrangementRepository $arrangementRepository */
    public $arrangementRepository;
    /** @var InformationRepository $informationRepository */
    public $informationRepository;

    public function __construct(
        ArrangementRepository $arrangementRepository,
        InformationRepository $informationRepository,
    )
    {
        $this->arrangementRepository = $arrangementRepository;
        $this->informationRepository = $informationRepository;
    }

    public function calculate($absorbtion, FloralArrangement $floralArrangement){
        $floralArrangementAbsorbtion = 0;
        $arrangements = $this->arrangementRepository->getAllArrangementsFlowers($floralArrangement->getId());
        /** @var Arrangement $arrangement */
        foreach ($arrangements as $arrangement){
            $informations = $this->informationRepository->getFlowerAbsorbtion($arrangement->getFlower()); // <-getID ?
            /** @var Information $information */
            foreach ($informations as $information){
                $floralArrangementAbsorbtion += $information->getValue() * $arrangement->getQuantity();
            }
        }
        $exponant = floor($absorbtion / $floralArrangementAbsorbtion);
        //il reste à multiplier l'exposant par le nombre de fleur de la composition
        foreach ($arrangements as $arrangement){
            $comp[] = $exponant .''. $arrangement->getFlower()->getName();
        }

        return $comp;
    }
}