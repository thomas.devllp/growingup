<?php

namespace App\Service\DataManager;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

abstract class DataManager
{
    protected EntityManagerInterface $em;

    protected ?Request $request = null;

    /** @var ServiceEntityRepository */
    protected $repository;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack
    ) {
        $this->em      = $em;
        $this->request = $requestStack->getCurrentRequest();
    }

    /** @required */
    abstract public function setRepository();
}
