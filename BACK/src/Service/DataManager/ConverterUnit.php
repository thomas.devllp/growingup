<?php

namespace App\Service\DataManager;

use App\Repository\UnityRepository;
use Exception;

class ConverterUnit
{
    const ABSORBTION = ['kCO2/an', 'tCO2/an'];
    const SURFACE = ['m²', 'hectare'];
    const DISTANCE = ['mm', 'cm', 'm'];

    public function __construct(
        private UnityRepository $unityRepository
    ) {
    }

    public function convert($information)
    {
        $value = $information->getValue();
        $unity = $information->getUnity()->getName();

        $futurUnity = $unity;
        switch ($unity) {
                //Absorbtion
            case 'tCO2/an':
                $value = $value * 1000;
                $futurUnity = 'kCO2/an';
                break;

                //Taille
            case 'mm':
                $value = $value / 10;
                $futurUnity = 'cm';
                break;
            case 'm':
                $value = $value * 100;
                $futurUnity = 'cm';
                break;

                //Surface
            case 'hectare':
                $value = $value * 10000;
                $futurUnity = 'm²';
                break;
        }

        $unity = $this->unityRepository->findByName($futurUnity);

        if (is_null($unity)) {
            throw new Exception("Unity not found $futurUnity");
        }

        $information->setUnity($unity);
        $information->setValue($value);

        return $information;
    }
}
