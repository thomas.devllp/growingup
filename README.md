# GrowingUp

![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Symfony](https://img.shields.io/badge/symfony-%23000000.svg?style=for-the-badge&logo=symfony&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)

### GrowingUp is a project realized by 3 students of ECV Digital which allows to compensate its carbon footprint by placing

<br>

![Home growingUp page](./FRONT/src/assets/img/homePage.png)

## Getting started

```
git clone https://gitlab.com/thomas.devllp/growingup.git
```

- Go to back file and run

```
composer install
```

- Go to front file and run

```
npm install
```

- Update your .env file

```
DATABASE_URL="mysql://app:!ChangeMe!@127.0.0.1:3306/app?serverVersion=8&charset=utf8mb4"
```

## Start the project

### Front

```
cd FRONT
npm run start
```

### Back

```
cd BACK
symfony server:start
```

### Insert Fixtures

```
php bin/console app:insert-data
php bin/console app:insert-information
php bin/console app:insert-arrangement
```

### Can open API documentation at this url :

**http://localhost:8888/doc**

## Create a new branch for your feature

```
git checkout -b feature/your-feature
git push -u origin feature/your-feature
```

## Authors

- GABOT Thomas
- BARET Julien
- VANDENBERGHE Fabien
